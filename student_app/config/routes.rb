Rails.application.routes.draw do
  devise_for :users
  resources :studs
  resources :groups do 
    post 'groups/add_students' => 'groups#add_students', :as => :add_studs_group
  end
  resources :marks do
		collection do
			get 'reports'
		end
	end
  root 'studs#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
