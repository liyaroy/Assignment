class CreateStuds < ActiveRecord::Migration[5.0]
  def change
    create_table :studs do |t|
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
