class ChangeColumnName < ActiveRecord::Migration[5.0]
  def change
  	rename_column :marks, :subjects_id, :subject_id
  	rename_column :marks, :studs_id, :stud_id
  end
end
