class Stud < ApplicationRecord
	has_and_belongs_to_many :groups
	has_many :marks
end
