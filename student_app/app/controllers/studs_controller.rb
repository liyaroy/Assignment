class StudsController < ApplicationController
	def index
		@studs = Stud.all
	end
	
	def new
		@stud = Stud.new
	end

	def create
		@stud = Stud.new(stud_params)
		respond_to do|f|
			if @stud.save
				# redirect_to @stud
				# f.html { redirect_to @stud, notice: 'Student was successfully added' }
				@studs = Stud.all
				f.js {}
			else
				# render 'new'
				f.html { render 'new' }
				f.json { render json: @stud.errors, status: :unprocessable_entity}
			end
		end
	end

	def show
		@stud = Stud.find(params[:id])
	end

	def edit
	end

	def update
	end

	def delete
	end

	private
	def stud_params
		params.require(:stud).permit(:name, :email)
	end
end
