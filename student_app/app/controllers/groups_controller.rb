class GroupsController < ApplicationController
	def index
		@groups=Group.all
	end

	def new
		@group=Group.new
	end

	def create
		@group=Group.new(group_params)
		if @group.save
			redirect_to @group
		else
			render 'new'
		end
	end
	
	def show
		@group=Group.find(params[:id])
	end

	def add_students
    if params[:group_id]
     	puts params
      @group = Group.find params[:group_id]
      @stud = Stud.find params[:stud][:id]
      # binding.pry
      @group.studs << @stud
      redirect_to action:'index' 		
		end
	end

	private
	def group_params
		params.require(:group).permit(:name)
	end
end
