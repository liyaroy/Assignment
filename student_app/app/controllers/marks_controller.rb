class MarksController < ApplicationController
	def new
		@mark = Mark.new
	end

	def create
		respond_to do|f|
			@mark = Mark.new(mark_params)
			if @mark.save
				# redirect_to @mark
				f.html { redirect_to new_mark_path, notice: 'Mark was successfully added' }
			else
				render 'new'
			end
		end
	end

	def reports
		@student = Stud.find(params[:id])
		marks = Mark.where stud_id: @student.id
		@weighted_avg = 	Mark.calculation(marks)
	end

	private
	def mark_params
		params.require(:mark).permit(:stud_id, :subject_id, :mark)
	end
end
