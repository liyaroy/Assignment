FactoryGirl.define do
	factory :group, class: Group do
		sequence(:name) { |n| "name#{n}" }
 	end
end
