FactoryGirl.define do
	factory :mark1, class: Mark do
		mark 50
		subject_id 1
		stud_id 2
 	end

 	factory :mark2, class: Mark do
		mark 50
		subject_id 2
		stud_id 2
 	end

 	factory :mark3, class: Mark do
		mark 50
		subject_id 3
		stud_id 2
 	end
end