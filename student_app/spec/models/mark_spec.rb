require 'spec_helper'

RSpec.describe MarksController, type: :model do
	it 'calculate weighted average' do
		subject1 = FactoryGirl.create(:subject1)
		subject2 = FactoryGirl.create(:subject2)
		subject3 = FactoryGirl.create(:subject3)
		subject4 = FactoryGirl.create(:subject4)
		mark1 = FactoryGirl.create(:mark1)
		mark2 = FactoryGirl.create(:mark2)
		mark3 = FactoryGirl.create(:mark3)
		marks = Mark.where stud_id: 2
		@weighted_avg = Mark.calculation(marks)
		expect(@weighted_avg).to eq(11.25)
	end
end